import json
import requests
import threading
from flask import request
from flask import jsonify
from app.config import *
from app.menu import *
from app.models import *
from app import flask_app
from app import db
from time import sleep

data = {"url": WEBHOOK_URL}
url = f"{TELEGRAM_URL}/bot{BOT_TOKEN}/setWebhook"
bot_url = f"{TELEGRAM_URL}/bot{BOT_TOKEN}/"


def working_loop():
    while True:
        sleep(15)
        users = Users.query.all()
        for user in users:
            fleet = get_fleet(user)
            tank = get_tanks(user)
            airplane = get_airplanes(user)
            infantry = get_infantry(user)
            user.balance += infantry["count"] * infantry["income"] + tank["count"] * tank["income"] \
                                + airplane["count"] * airplane["income"] + fleet["count"] * fleet["income"]
        db.session.commit()


def log_info(text, filename='file.log'):
    with open(filename, 'w') as f:
        f.write(text)
        f.write('\n')


def get_tanks(user):
    for army in user.army:
        for tank in army.tanks:
            if army.id == tank.army_id:
                return {"count": tank.count, "income": tank.income}


def send_message(chat_id, text):
    chat_data = {'chat_id': chat_id, 'text': text}
    message_url = f'{TELEGRAM_URL}/bot{BOT_TOKEN}/sendMessage'
    requests.post(message_url, data=chat_data)


def create_menu(user_id, menu, message):
    headers = {"Content-type": "application/json"}
    chat_data = {"chat_id": user_id, "text": message}
    chat_data.update(menu)
    chat_data = json.dumps(chat_data)
    message_url = f"{TELEGRAM_URL}/bot{BOT_TOKEN}/sendMessage"
    requests.post(message_url, headers=headers, data=chat_data)


def create_button(message, user_id, callback_data):
    headers = {'Content-type': 'application/json'}
    chat_data = {'chat_id': user_id,
                 'text': message,
                 'reply_markup':
                     {'inline-keyboard': [[{'text': 'nig', 'callback_data': callback_data}]]}}
    chat_data = json.dumps(chat_data)
    message_url = f'{TELEGRAM_URL}/bot{BOT_TOKEN}/sendMessage'
    result = requests.post(message_url, headers=headers, data=chat_data)
    print(result)
    pass


def delete_message(chat_id, message_id):
    delete_url = f'{TELEGRAM_URL}/bot{BOT_TOKEN}/deleteMessage'
    chat_data = {'chat_id': chat_id, 'message_id': message_id}
    requests.post(delete_url, data=chat_data)


def edit_message(message, chat_id, message_id, menu):
    chat_data = {"chat_id": chat_id, "message_id": message_id, "text": message}
    data.update(menu)
    message_url = f"{TELEGRAM_URL}/bot{BOT_TOKEN}/editMessage"
    requests.post(message_url, data=chat_data)


def info_about_army(user, chat_id):
    text = ""
    for army in user.army:
        for fleet in army.fleet:
            if fleet.army_id == army.id:
                text += f"Amount of fleet: {fleet.count}. Price per item:{int(fleet.price)}$\n"
        for infantry in army.infantry:
            if infantry.army_id == army.id:
                text += f"Amount of infantry: {infantry.count}. Price per item:{int(infantry.price)}$\n"
        for tank in army.tanks:
            if tank.army_id == army.id:
                text += f"Amount of tanks: {tank.count}. Price per item: {int(tank.price)}$\n"
        for airplane in army.airplanes:
            if airplane.army_id == army.id:
                text += f"Amount of airplanes: {airplane.count}. Price per item: {int(airplane.price)}$\n"
    send_message(chat_id, text=text)


def get_infantry(user):
    for army in user.army:
        for infantry in army.infantry:
            if army.id == infantry.army_id:
                return {"count": infantry.count, "income": infantry.income}


def get_fleet(user):
    for army in user.army:
        for fleet in army.fleet:
            if army.id == fleet.army_id:
                return {"count": fleet.count, "income": fleet.income}


def buy_fleet(user, chat_id):
    local_balance = user.balance
    for army in user.army:
        for fleet in army.fleet:
            if fleet.army_id == army.id and user.balance >= fleet.price:
                fleet.count += 1
                user.balance -= fleet.price
    if local_balance != user.balance:
        send_message(chat_id, text="Fleet was bought!")
    else:
        send_message(chat_id, text="Not enough money for fleet(")


def buy_tank(user, chat_id):
    local_balance = user.balance
    for army in user.army:
        for tank in army.tanks:
            if tank.army_id == army.id and user.balance >= tank.price:
                tank.count += 1
                user.balance -= tank.price
    if local_balance != user.balance:
        send_message(chat_id, text="Tank was bought!")
    else:
        send_message(chat_id, text="Not enough money for tank(")


def buy_airplane(user, chat_id):
    local_balance = user.balance
    for army in user.army:
        for airplane in army.airplanes:
            if airplane.army_id == army.id and user.balance >= airplane.price:
                airplane.count += 1
                user.balance -= airplane.price
    if local_balance != user.balance:
        send_message(chat_id, text="Airplane was bought!")
    else:
        send_message(chat_id, text="Not enough money for airplane(")


def buy_infantry(user, chat_id):
    local_balance = user.balance
    for army in user.army:
        for infantry in army.infantry:
            if infantry.army_id == army.id and user.balance >= infantry.price:
                infantry.count += 1
                user.balance -= infantry.price
    if local_balance != user.balance:
        send_message(chat_id, text="Infantry was bought!")
    else:
        send_message(chat_id, text="Not enough money for infantry(")


def admin_join_info(chat_id):
    send_message(chat_id, text="result")


def get_airplanes(user):
    for army in user.army:
        for airplanes in army.airplanes:
            if army.id == airplanes.army_id:
                return {"count": airplanes.count, "income": airplanes.income}


def processing_button():
    user_id = request.json["callback_query"]["from"]["id"]
    chat_id = request.json["callback_query"]["message"]["chat"]["id"]
    rdata = request.json["callback_query"]["data"]
    try:
        user = Users.query.get(int(user_id))
        if rdata == "BalanceMenu":
            send_message(chat_id=chat_id, text=balance_info(user))
        elif rdata == "ArmyMenu":
            create_menu(user_id=user_id, message='Army menu', menu=army_menu())
        elif rdata == "BackMenu":
            create_menu(chat_id, main_menu(), "Main menu")
        elif rdata == "AdminMenu":
            if user.is_admin == 1:
                create_menu(chat_id, admin_menu(), "Special admin menu")
            else:
                send_message(chat_id=chat_id, text="You doesn't have administrator rights!")
        elif rdata == "BuyFleet":
            buy_fleet(user=user, chat_id=chat_id)
        elif rdata == "BuyTank":
            buy_tank(user=user, chat_id=chat_id)
        elif rdata == "BuyAirplane":
            buy_airplane(user=user, chat_id=chat_id)
        elif rdata == "BuyInfantry":
            buy_infantry(user=user, chat_id=chat_id)
        elif rdata == "AdminInfo":
            admin_join_info(chat_id=chat_id)
        elif rdata == "ChangeBalance":
            send_message(chat_id, text="This action will recharge your balance with 100$")
            user.balance += 100
        elif rdata == "InfoMenu":
            info_about_army(user=user, chat_id=chat_id)
        db.session.commit()
    except (RuntimeError, TypeError, NameError, AttributeError):
        send_message(chat_id, text="Server error")


def balance_info(user):
    return f'Balance of current user in $dollars$: {user.balance}'


@flask_app.route('/login', methods=['POST'])
def index():
    if "callback_query" in request.json:
        processing_button()
    if "message" in request.json:
        if "game" in request.json["message"]:
            return "OK"
        if "text" in request.json["message"]:
            user_id = request.json["message"]["from"]["id"]
            username = request.json["message"]["from"]["username"]
            user = Users.query.get(int(user_id))
            if user is None:
                if request.json["message"]["text"] == "/start":
                    user = Users(id=int(user_id), balance=50, username=username, is_admin=0)
                    army = Army(user_id=int(user_id))
                    db.session.add(user)
                    db.session.commit()
                    db.session.add(army)
                    db.session.commit()
                    fleet = Fleet(price=2500, income=200, army_id=army.id, count=0)
                    tanks = Tanks(price=100, income=5, army_id=army.id, count=0)
                    airplanes = Airplanes(price=1000, income=75, army_id=army.id, count=0)
                    infantry = Infantry(price=25, income=1, army_id=army.id, count=0)
                    db.session.add(fleet)
                    db.session.add(tanks)
                    db.session.add(airplanes)
                    db.session.add(infantry)
                    db.session.commit()
                    create_menu(user_id, main_menu(), "Main menu")
                else:
                    send_message(
                        "Welcome to Gold Mine. Write /start to start",
                        user_id)
        return jsonify(request.json)
    return '<h1>Hello on Linux!</h1>'


def set_webhook():
    requests.post(url, data)


if __name__ == '__main__':
    set_webhook()
    x = threading.Thread(target=working_loop)
    x.start()
    flask_app.run(host='0.0.0.0')
